<?php
namespace Mkprod\MkBundle\Util;

/**
 * Description of ButtonUtil
 *
 * @author michi
 */
class ButtonUtil {
    
    static $icons = array(
            "bash" => "bundles/mkprodmk/images/icons/languages/bash.png",
            "html" => "bundles/mkprodmk/images/icons/languages/html.png",
            "css"  => "bundles/mkprodmk/images/icons/languages/css.png",
            "js"   => "bundles/mkprodmk/images/icons/languages/js.png",
            "cs"  => "bundles/mkprodmk/images/icons/languages/cs.png",
            "cpp"  => "bundles/mkprodmk/images/icons/languages/cpp.png",
            "java" => "bundles/mkprodmk/images/icons/languages/java.gif",
            "hibernate" => "bundles/mkprodmk/images/icons/languages/hibernate.png",
            "junit"   => "bundles/mkprodmk/images/icons/languages/junit.gif",
            "qt"      => "bundles/mkprodmk/images/icons/languages/qt.jpg",
            "oracle"  => "bundles/mkprodmk/images/icons/languages/oracle.jpg",
            "mssql"   => "bundles/mkprodmk/images/icons/languages/mssql.png",
            "php"     => "bundles/mkprodmk/images/icons/languages/php.png",
            "symfony" => "bundles/mkprodmk/images/icons/languages/symfony.png",
            "linuxmint" => "bundles/mkprodmk/images/icons/languages/linuxmint.jpg",
            "ubuntu"   => "bundles/mkprodmk/images/icons/languages/ubuntu.png",
            "gimp"     => "bundles/mkprodmk/images/icons/languages/gimp.png",
            "git"      => "bundles/mkprodmk/images/icons/languages/git.png",
            "eclipse"  => "bundles/mkprodmk/images/icons/languages/eclipse.png",
            "netbeans" => "bundles/mkprodmk/images/icons/languages/netbeans.png",
            "aptana"   => "bundles/mkprodmk/images/icons/languages/aptana.png",
            "visualstudio" => "bundles/mkprodmk/images/icons/languages/visualstudio.png",
            "pdf"      => "bundles/mkprodmk/images/icons/pdf.png",
            "bitbucket" => "bundles/mkprodmk/images/icons/languages/bitbucket.png",
            "down"     =>  "bundles/mkprodmk/images/icons/arrow-down.png",
            "book"     => "bundles/mkprodmk/images/icons/book.png",
            "mail"     => "bundles/mkprodmk/images/icons/mail.png",
            "sweetmail" => "bundles/mkprodmk/images/icons/sweet_mail.gif",
            "globe"     => "bundles/mkprodmk/images/icons/globe.png",
            "code"      => "bundles/mkprodmk/images/icons/code.png",
            "facebook"  => "bundles/mkprodmk/images/icons/facebook.png",
            "twitter"   => "bundles/mkprodmk/images/icons/twitter.png",
            "google"    => "bundles/mkprodmk/images/icons/google.png"
    );
    
    
    static function getIcon($key) {
        return self::$icons[$key];
    }
    
    
    static function getIcons() {
        return self::$icons;
    }
}

?>
