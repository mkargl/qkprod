<?php

namespace Mkprod\MkBundle\Auxiliary;

use Mkprod\MkBundle\Auxiliary\Image;
use Mkprod\MkBundle\Exception\IllegalDataTypeException;

/**
 * Test class for Project.
 * Generated by PHPUnit on 2013-03-17 at 12:57:53.
 */
class ProjectTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var Project
     */
    protected $project;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp() {
        $this->project = new Project;
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown() {
        
    }

    /**
     * @covers Mkprod\MkBundle\Auxiliary\Project::getTitle
     * @covers Mkprod\MkBundle\Auxiliary\Project::setTitle
     */
    public function testSetGetTitle() {
        // Remove the following lines when you implement this test.
        $expected="testtitle";
        $result = $this->project->setTitle($expected)->getTitle();
        
        $this->assertEquals($expected, $result);
    }
    
    /**
     * Provides 1 Argument of each datatype
     * @return array of parameters used by phpunit
     */
    public function dataTypeProvider() {
        return array(
          array("teststring"),
          array(123), 
          array(array()),
          array(new Image()),
          array(null), array(123.0)
        );
    }
    
    /**
     * @dataProvider dataTypeProvider
     * @param type $title
     */
    public function testSetTitleExpectingIllegalArgumentException($title) {
        if(is_string($title)) {
            //ignore strings as they are not supposed to rise exceptions
            return;
        }
        
        try {
            $this->project->setTitle($title);
        } catch(IllegalDataTypeException $ex) {
            return;
        } catch(\Exception $ex) {
            $this->fail(sprintf("Expected IllegalDataTypeException but got 
                [%s] with message [%s]", get_class($ex), $ex->getMessage()));
        }
        
        $this->fail("Exception of type IllegalDataTypeException expected");
    }


    /**
     * @covers Mkprod\MkBundle\Auxiliary\Project::getDesc
     * @covers Mkprod\MkBundle\Auxiliary\Project::setDesc
     */
    public function testSetGetDesc() {
        $expected="testdesc";
        $result = $this->project->setDesc($expected)->getDesc();
        
        $this->assertEquals($expected, $result);
    }
    
    
    /**
     * @dataProvider dataTypeProvider
     * @param type $desc
     */
    public function testSetDescExpectingIllegalDataTypeException($desc) {
        if(is_string($desc)) {
            //ignore strings as they are not supposed to rise exceptions
            return;
        }
        
        try {
            $this->project->setDesc($desc);
        } catch(IllegalDataTypeException $ex) {
            return;
        } catch(\Exception $ex) {
            $this->fail(sprintf("Expected IllegalDataTypeException but got 
                [%s] with message [%s]", get_class($ex), $ex->getMessage()));
        }
        
        $this->fail("Exception of type IllegalDataTypeException expected");
    }

    
    /**
     * @covers Mkprod\MkBundle\Auxiliary\Project::getUrl
     * @covers Mkprod\MkBundle\Auxiliary\Project::setUrl
     */
    public function testSetGetUrl() {
        $expected="testurl";
        $result = $this->project->setUrl($expected)->getUrl();
        
        $this->assertEquals($expected, $result);
    }
    
    
        /**
     * @dataProvider dataTypeProvider
     * @param type $url
     */
    public function testSetUrlExpectingIllegalDataTypeException($url) {
        if(is_string($url)) {
            //ignore strings as they are not supposed to rise exceptions
            return;
        }
        
        try {
            $this->project->setUrl($url);
        } catch(IllegalDataTypeException $ex) {
            return;
        } catch(\Exception $ex) {
            $this->fail(sprintf("Expected IllegalDataTypeException but got 
                [%s] with message [%s]", get_class($ex), $ex->getMessage()));
        }
        
        $this->fail("Exception of type IllegalDataTypeException expected");
    }
    


    /**
     * @covers Mkprod\MkBundle\Auxiliary\Project::setImages
     * @covers Mkprod\MkBundle\Auxiliary\Project::getImages
     */
    public function testSetGetImages() {
        $expected = array(new Image());
        $result = $this->project->setImages($expected)->getImages();
        
        $this->assertSame($expected, $result);
    }
    
    
    public function testSetImagesExpectingIllegalDataTypeExceptionProvider() {
        return array(
            array(array(null)),
            array(array(123)),
            array(array("asd")),
            array(array(array())),
            array(array(new Image(), "a")),
            array(array(new Image(), new Project()))
        );
    }
    
    
    /**
     * @dataProvider testSetImagesExpectingIllegalDataTypeExceptionProvider
     */
    public function testSetImagesExpectingIllegalDataTypeException($images) {
        
        try {
            $this->project->setImages($images);
        } catch(IllegalDataTypeException $ex) {
            return;
        } catch(\Exception $ex) {
            $this->fail(sprintf("Expected IllegalDataTypeException but got 
                [%s] with message [%s]", get_class($ex), $ex->getMessage()));
        }
         $this->fail("Exception of type IllegalDataTypeException expected");
    }
}

?>
