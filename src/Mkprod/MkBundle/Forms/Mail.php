<?php
namespace Mkprod\MkBundle\Forms;

use Mkprod\MkBundle\Exception\IllegalValueException;
use Mkprod\MkBundle\Exception\IllegalDataTypeException;

   
class Mail {
   
   /**
    * @var string
    */
   private $body = "";
   
   /**
    *
    * @var string
    */
   private $sender = "";
   
   /**
    *
    * @var string
    */
   private $receiver = "";
   
   /**
    * @var string
    */
   private $subject = "";
   
   
   
   /**
    * 
    */
   public function __construct() { 
   }
   
   
   
   /**
    * @return string
    */
   public function getBody() {
      return $this->body;
   }

   /**
    * 
    * @param string $body
    * @return \Mail
    */
   public function setBody($body) {
      
      if(!is_string($body)) {
         throw new IllegalDataTypeException(
                 sprintf("[body] with type [%s] must be [string]",
                 is_object($body)?get_class($body):gettype($body)));
      }
      
      $this->body = $body;
      return $this;
   }

   /**
    * @return string
    */
   public function getSender() {
      return $this->sender;
   }

   /**
    * @param string $sender
    * @return \Mail
    */
   public function setSender($sender) {
     if($this->checkMailParameters($sender)) {
         $this->sender = $sender;       
      }
      return $this;
   }

   /**
    * @return string
    */
   public function getReceiver() {
      return $this->receiver;
   }

   
   /**
    * @param string $receiver
    * @return \Mail
    */
   public function setReceiver($receiver) {
      if($this->checkMailParameters($receiver)) {
         $this->receiver = $receiver;
      }
      return $this;
   }
   
   /**
    * @return string
    */
   public function getSubject() {
      return $this->subject;
   }

   /**
    * @param type $subject
    * @return \Qkprod\MangressBundle\Forms\Mail
    */
   public function setSubject($subject) {
      $this->subject = $subject;
      return $this;
   }
      
   
   /**
    * Validates email address
    * @return boolean
    * @throws IllegalArgumentException
    */
   public function checkMailParameters($email) {
      
      // email address must be string
      if(!is_string($email)) {
         throw new IllegalValueException(
                 sprintf("[email] with type [%s] must be [string]",
                 is_object($email)?get_class($email):gettype($email)));
      }
      
      // email address must not be empty
      if(empty($email)) {
         throw new IllegalValueException("[email] must not be empty");
      }
      
      // email address must be of format ~~~ [A-Z]@[A-Z].[A-Z]
      if (!preg_match("/.+\@.+\..+/", $email)){
	 throw new IllegalValueException(
                 "[email] with value [$email] must be valid email address");
      }
      
      return true;
   }
   
   
   public function __toString() {
      return sprintf("Sender [%s] , Receiver [%s] , Body [%s]",
                     $this->sender, $this->receiver, $this->body);
   }
}

?>
