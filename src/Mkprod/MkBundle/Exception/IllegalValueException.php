<?php
namespace Mkprod\MkBundle\Exception;


/**
 * Raised when:
 *   An Object needs initialization but gets unly arguments not allowed
 *   by this object (does not take nulls, or ""s, or -1)
 * 
 * @author Michael Kargl
 */
class IllegalValueException extends \Exception{
   
   private $offender;   // the object throwing the exception
   
   
   /**
    * Create a new \Exception
    * @param string     $message   Message giving more details about the cause
    * @param mixed      $offender  Object causing the exception
    * @param number     $code      Exception-errorcode 
    * @param Exception  $previous  Previous exception if nested)
    */
   public function __construct($message = "", $offender = null, 
                               $code = 0,     $previous = null) {
      parent::__construct($message, $code, $previous);
      $this->offender = $offender;
   }
   
   /**
    * Get the object that has thrown the exception
    * @return Mixed
    */
   public function getOffender() {
      return $this->offender;
   }
   
   /**
    * @param Mixed $thrower
    * @return \Qkprod\MangressBundle\Exception\IllegalArgumentException
    */
   public function setOffender($offender) {
      $this->offender = $offender;
      return $this;
   }
   
}

?>
