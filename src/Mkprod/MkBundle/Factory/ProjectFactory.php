<?php
namespace Mkprod\MkBundle\Factory;

use Mkprod\MkBundle\Auxiliary\Project;

/**
 * Description of ProjectFactory
 *
 * @author michi
 */
class ProjectFactory {
    
    
    public function produce($title) {
        $product = new Project();
        $product->setTitle($title);
        
        return $product;
    }
        
}
    
    

?>
