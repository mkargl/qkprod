<?php

namespace Mkprod\MkBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class TestController extends Controller {
    /**
     * @Route("/")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function mainAction() {
        return new Response("mainAction");
    }
    
    /**
     * @Route("/mail", name="test_mail")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function aboutAction() {
        $mailFactory = new \Mkprod\MkBundle\Factory\MailFactory();
        $mail = $mailFactory->buildMailFactory(
                "sender@mail.com",
                "receiver@mail.com",
                "bodybody <3",
                "subject <3");

        return $this->render("MkprodMkBundle:Mail:email.html.twig",
                array("mail" => $mail));
    }
}
