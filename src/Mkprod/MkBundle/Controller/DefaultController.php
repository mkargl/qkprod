<?php

namespace Mkprod\MkBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Mkprod\MkBundle\Util\ButtonUtil;


class DefaultController extends Controller {
    /**
     * @Route("/")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction() {
        return $this->mainAction();
    }
    
    
    /**
     * @Route("/main", name="mkprodmk_main")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function mainAction() {
        return $this->render("MkprodMkBundle:Default:main.html.twig", 
                             array("icons" => ButtonUtil::$icons));
    }
}
