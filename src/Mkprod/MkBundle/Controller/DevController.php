<?php

namespace Mkprod\MkBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Mkprod\MkBundle\Util\ButtonUtil;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Mkprod\MkBundle\Factory\MailFactory;

class DevController extends Controller {

    /**
     * Renders the main page with all developers
     * @Route("/devs", name="mkbundle_dev_main")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function mainAction() {
        return $this->render("MkprodMkBundle:Devs:devpage.html.twig", array(
                    "icon" => ButtonUtil::getIcons()
        ));
    }

    /**
     * Renders one developer
     * @param string $devname
     * @Route("/dev/{devname}", name="mkbundle_dev_show")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($devname) {
        $mailFactory = new MailFactory();
        $mail = $mailFactory->buildMailFactory(
                "your@mail.here", "michaelkargl@qkprod.com", "");

        /* CREATE MAIL FORM AND LINK IT TO OBJECT */
        $mailform = $this->createFormBuilder($mail)
                ->add("sender", "email")
                ->add("subject", "text")
                ->add("body", "textarea")
                ->getForm();

        // Form submitted
        $mails_sent = -1;
        $request = $this->getRequest();
        if ($request->isMethod("POST")) {
            // bind object to current request(i.e a user)
            $mailform->bind($request);

            if ($mailform->isValid()) {
                // returns amount of successfully sent messages

                $message = \Swift_Message::newInstance()
                        ->setSubject($mail->getSubject())
                        ->setFrom($mail->getSender())
                        ->setTo(array("michaelkargl@qkprod.com", "michaelkargl@outlook.com"))
                        ->setBody($this->renderView(
                                "MkprodMkBundle:Mail:email.html.twig", array(
                                    "mail" => $mail)), 
                                "text/html", "utf-8");
                $mails_sent = $this->get("mailer")->send($message);
            } else {
                return new Response("mailform invalid");
            }
        }
        return $this->render("MkprodMkBundle:Devs:$devname.html.twig", array(
            "icons" => ButtonUtil::getIcons(),
            "mailform" => $mailform->createView(),
            "mails_sent" => $mails_sent));
    }
}
