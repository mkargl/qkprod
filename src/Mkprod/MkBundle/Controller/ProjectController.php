<?php

namespace Mkprod\MkBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Mkprod\MkBundle\Util\ButtonUtil;


class ProjectController extends Controller {
    
    
    /**
     * @Route("/", name="mkprodmk_project_main")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function mainAction() {
        return $this->render("MkprodMkBundle:Project:projects.html.twig",
                array("icons" => ButtonUtil::$icons));
    }
    
    /**
     * @Route("/{project}", name="mkprodmk_project_show")
     */
    public function showAction($project) {
        return $this->render("MkprodMkBundle:Project:$project.html.twig", 
                array("icons" => ButtonUtil::$icons));
    }
}
