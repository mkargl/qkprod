<?php

namespace Mkprod\MkBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class BlogController extends Controller {

      
    /**
     * @Route("/blog", name="mkprodmk_blog_blog")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function blogAction() {
        return $this->forward("BloggerBlogBundle:Page:index");
    }
    
}
