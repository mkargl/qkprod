<?php

namespace Mkprod\MkBundle\Auxiliary;

/**
 * Description of Image
 *
 * @author michi
 */
class Image {
   
    /**
     * @var string
     */
    private $title;
    
    
    /**
     * @var string
     */
    private $desc;
    
    /**
     * @var string
     */
    private $thumb;
    
    /**
     * @var string
     */
    private $image;
    
    /**
     * @var string
     */
    private $url;
    
    
        /**
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }
    
    /**
     * @param string $title
     * @return \Mkprod\MkBundle\Auxiliary\Image
     */
    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getDesc() {
        return $this->desc;
    }
    
    /**
     * @param string $desc
     * @return \Mkprod\MkBundle\Auxiliary\Image
     */
    public function setDesc($desc) {
        $this->desc = $desc;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getUrl() {
        return $this->url;
    }
    
    /**
     * @param string $url
     * @return \Mkprod\MkBundle\Auxiliary\Image
     */
    public function setUrl($url) {
        $this->url = $url;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getThumb() {
        return $this->thumb;
    }
    
    /**
     * @param string $thumb
     * @return \Mkprod\MkBundle\Auxiliary\Image
     */
    public function setThumb($thumb) {
        $this->thumb = $thumb;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getImage() {
        return $this->image;
    }
    
    /**
     * 
     * @param string $image
     * @return \Mkprod\MkBundle\Auxiliary\Image
     */
    public function setImage($image) {
        $this->image = $image;
        return $this;
    }
    
}

?>
