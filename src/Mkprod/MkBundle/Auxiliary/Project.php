<?php

namespace Mkprod\MkBundle\Auxiliary;

use \Mkprod\MkBundle\Exception\IllegalDataTypeException;


/**
 * Description of Project
 *
 * @author michi
 */
class Project {
    
    /**
     * @var string 
     */
    private $title="";
    
    /**
     *
     * @var string
     */
    private $desc="";
    
    /**
     *
     * @var string
     */
    private $url="";
    
    
    /*
     * @var array
     */
    private $images=array();
    
    
    
    /**
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }
    
    /**
     * @param string $title
     * @return \Mkprod\MkBundle\Auxiliary\Project
     */
    public function setTitle($title) {
        if(!is_string($title)) {
            throw new IllegalDataTypeException(
                    "[title] must not be of any other type than string");
        }
        
        $this->title = $title;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getDesc() {
        return $this->desc;
    }
    
    /**
     * @param string $desc
     * @return \Mkprod\MkBundle\Auxiliary\Project
     */
    public function setDesc($desc) {
        if(!is_string($desc)) {
            throw new IllegalDataTypeException(
                    "[desc] must not be of any other type than string");
        }
        
        $this->desc = $desc;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getUrl() {
        return $this->url;
    }
    
    /**
     * @param string $url
     * @return \Mkprod\MkBundle\Auxiliary\Project
     */
    public function setUrl($url) {
        if(!is_string($url)) {
            throw new IllegalDataTypeException(
                    "[url] must not be of any other type than string");
        }
        
        $this->url = $url;
        return $this;
    }
    
    /**
     * @return array<Image>
     */
    public function getImages() {
        return $this->images;
    }
    
    /**
     * @param array<Image> $images
     * @return \Mkprod\MkBundle\Auxiliary\Project
     */
    public function setImages($images) {

        if(!is_array($images)) {
            throw new IllegalDataTypeException("[images] must be of type Array");
        }
        
        for($i=0; $i<count($images); $i++) {
            if(!is_object($images[$i]) || 
               get_class($images[$i]) != "Mkprod\MkBundle\Auxiliary\Image") {
                throw new IllegalDataTypeException("[images[$i] must be of type
                        \Mkprod\MkBundle\Auxiliary\Image");
            }
        }
        
        $this->images = $images;
        return $this;
    }
}

?>
