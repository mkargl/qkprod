<?php
namespace Mkprod\MangressBundle\Forms;


use Mkprod\MangressBundle\Forms\Mail;


/**
 * Description of MailFactory
 *
 * @author Michael Kargl
 * @email  michaelkargl@outlook.com
 */
class MailFactory {
   
   
   public function buildMailFactory($sender, $receiver, $body, $subject = "") {
         $mail = new Mail();
         $mail->setBody($body)
              ->setReceiver($receiver)
              ->setSender($sender)
              ->setSubject($subject);
         return $mail;
   }
   
}

?>
