<?php

namespace Mkprod\MangressBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Mkprod\MangressBundle\Util\ProgressBar;
use Mkprod\MangressBundle\Util\ProgressBarFactory;

use Mkprod\MangressBundle\Forms\Mail;
use Mkprod\MangressBundle\Forms\MailFactory;



class DefaultController extends Controller {

    /**
     * Merges all sections into one array
     * @param array $sections
     * @return array of progressbars
     */
    private function mergeSections(array $sections) {
        $allSections = array();

        foreach ($sections as $title => $section) {
            $allSections = array_merge($allSections, $section);
        }

        return $allSections;
    }

    /**
     * Fills array with progressbars for each section
     * @return array: Array with sections containing progressbars
     */
    private function getProgressSections() {
        $factory = new ProgressBarFactory();

        $internal = array(
            $factory->buildProgressBar(60, "Database Design"),
            $factory->buildProgressBar(00, "Data acquisition"),
            $factory->buildProgressBar(00, "Mangaeden API Layer"),
            $factory->buildProgressBar(00, "MAL API Layer")
        );

        $website = array(
            $factory->buildProgressBar(20, "General Layout"),
            $factory->buildProgressBar(0, "Progress"),
            $factory->buildProgressBar(0, "Info/Overview"),
            $factory->buildProgressBar(20, "Preview"),
            $factory->buildProgressBar(0, "Signup"),
            $factory->buildProgressBar(0, "Mangaprogress")
        );

        $phase = array(
            $factory->buildProgressBar(0, "Prototype"),
            $factory->buildProgressBar(0, "ALPHA")
        );

        /** Combine progressbar sections to one array * */
        return array(
            "Internal Progress" => $internal,
            "Site Progress" => $website,
            "Stages" => $phase
        );
    }

    /**
     * @Route("/")
     */
    public function indexAction() {
        
        return $this->mainAction();
    }

    /**
     * @Route("/main", name="main")
     */
    public function mainAction() {
        $factory = new ProgressBarFactory();

        $sections = $this->getProgressSections();
        $allSections = $this->mergeSections($sections);

        $totalProgressbar = $factory->buildProgressBarSummary(
                $allSections, "Current Progress");

        return $this->render("MkprodMangressBundle:Default:main.html.twig", 
                array("totalProgress" => $totalProgressbar));
    }

    /**
     * @Route("/progress", name="progress")
     */
    public function progressAction() {
        $sections = $this->getProgressSections();
        $allSections = $this->mergeSections($sections);

        $factory = new ProgressBarFactory();
        $total = $factory->buildProgressBarSummary($allSections, "Total");

        // render progressbars
        return $this->render("MkprodMangressBundle:Default:progress.html.twig",
                array("progressbars" => $sections, "total" => $total,
            "classie" => get_class($total)));
    }

    /**
     * @Route("/preview", name="preview")
     */
    public function previewAction() {
        return $this->render("MkprodMangressBundle:Default:preview.html.twig",
                array());
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contactAction(Request $request) {
        $factory = new MailFactory();

        // $sender, $receiver, $body, $subject
        $mail = $factory->buildMailFactory(
                "sender@email.address", "michaelkargl@outlook.com", "body", "subject");

        // create Form from FormEntity
        $form = $this->createFormBuilder($mail)
                ->add("subject", "text")
                ->add("sender", "email")
                ->add("receiver", "email", array("disabled" => true))
                ->add("body", "textarea")
                ->getForm();


        // form has been submitted
        if ($request->isMethod("POST")) {
            // bind request to mail-object
            $form->bindRequest($request);

            if ($form->isValid()) {
                return new Response("[$mail] successfully submitted");
            } else {
                return new Response("Form is not valid");
            }
        }

        return $this->render(
                        'MkprodMangressBundle:Default:contact.html.twig',
                array('form' => $form->createView()));
    }

}
