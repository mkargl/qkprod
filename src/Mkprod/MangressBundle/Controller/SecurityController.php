<?php

namespace Mkprod\MangressBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class SecurityController extends Controller {
    
    
    /**
     * @Route("/mangress_login", name="mangress_login")
     * @return type
     */
    public function loginAction() {
        $request = $this->getRequest();
        $session = $request->getSession();
        
        
        // retrieve error if existing
        $error = null;
        if($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }
      
        return $this->render(
                "MkprodMangressBundle:Security:login.html.twig",
                array("last_username" => $session->get(SecurityContext::LAST_USERNAME),
                      "error"         => $error));
    }
    
    
    /**
     * @Route("/mangress_login_check", name="mangress_login_check")
     */
    public function login_checkAction() {
        // The security layer will intercept this request
        // and manage authentication by itself
    }
    
    
    /**
     * @Route("/mangress_logout", name="mangress_logout")
     */
    public function logoutAction() {
        // The security layer will intercept this request
        // and manage logout by itself
    }
}

?>
