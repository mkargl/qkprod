<?php
namespace Mkprod\MangressBundle\Util;

use Mkprod\MangressBundle\Exception\IllegalArgumentException;


/**
 * Creates ProgressBar instances
 *
 * @author Michael Kargl
 * @email  michaelkargl@outlook.com
 */
class ProgressBarFactory {
   
   
   /**
    * Creates a ProgressBar instance
    * @param float|int $progress   progress in percent
    * @param string    $title      title of progressbar
    * @return \Mkprod\MangressBundle\Util\ProgressBar
    * @throws IllegalArgumentException:
    *       thrown if invalid parameters have been used
    */
   public function buildProgressBar($progress, $title = "") {
      if(!is_string($title)){
         throw new IllegalArgumentException(
         sprintf("Field [title] with type [%s] must be string",
                  is_object($title)?get_class($title):gettype($title)));
      }
      if(!is_float($progress) && !is_int($progress)) {
         throw new IllegalArgumentException(
         sprintf("Field [progress] with type [%s] must be [float or int]",
                 is_object($progress)?get_class($progress):gettype($progress)));
      }
      if($progress < 0 || $progress > 100) {
         throw new IllegalArgumentException(
                 "Field [progress] must be between 0 and 100");
      }
      
      $progressbar = new ProgressBar($progress, $title);
      
      return $progressbar;
   }
           
   
   
   /**
    * 
    * @param array $progressbars    Array of ProgressBar-instances
    * @param string $title
    * @return ProgressBar instance
    * @throws IllegalArgumentException:
    *       thrown by buildProgressBar() if invalid parameters are being used 
    */
   public function buildProgressBarSummary(array $progressbars, $title = "") {
         if(empty($progressbars)) {
            throw new IllegalArgumentException(
                    "ProgressBar-array must not be empty");
         }
         
         // check types of array content
         for($i=0; $i<count($progressbars); $i++) {
            $bar = $progressbars[$i];
            
            if(!is_object($bar)) {
               throw new IllegalArgumentException(
                       "Element progressbars[$i] with type [%s] has to be".
                       " Mkprod\MangressBundle\Util\ProgressBar", gettype($bar));
            }
            
            if(get_class($bar) != "Mkprod\MangressBundle\Util\ProgressBar") {
               throw new IllegalArgumentException(
                       sprintf("Element [] of array progressbars".
                               " with type [%s] must be ProgressBar", 
                               get_class($bar)));
            }
         }
         
      
      
         $sum = 0;   // progress of all bars together
         foreach($progressbars as $bar) {
            $sum += $bar->getProgress();
         }
         $percent = $sum/count($progressbars);
          
         return $this->buildProgressBar($percent, $title);
   }
   
   
   
}

?>
