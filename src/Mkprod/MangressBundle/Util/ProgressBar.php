<?php
namespace Mkprod\MangressBundle\Util;


use Mkprod\MangressBundle\Exception\IllegalArgumentException;

/**
 * Represents one progressbar
 * @author Michael Kargl
 * @email  michaelkargl@outlook.com
 */
class ProgressBar {
   
   /**
    * Title of progressbar
    * @var string
    */
   private $title;
   
   /**
    * Progress in percent
    * @var float
    */
   private $progress;
   
   
   /**
    * @param float|int $progress
    * @param string    $title
    */
   function __construct($progress, $title) {
      $this->title = $title;
      $this->progress = $progress;
   }

   
   
   /**
    * @return string
    */
   public function getTitle() {
      return $this->title;
   }

   /**
    * @param string $title
    * @return ProgressBar: reference to initialized object
    */
   public function setTitle($title) {
      
      // TYPECHECK PARAMETERS
      if(!is_string($title)) {
         throw new IllegalArgumentException(
               sprintf("Field [title] with type [%s] must be [string]",
               is_object($title)?get_class($title):gettype($title)
         ));
      }
      
      $this->title = $title;
      return $this;
   }

   /**
    * @return float
    */
   public function getProgress() {
      return $this->progress;
   }

   /**
    * @param float $progress
    * @return ProgressBar reference to initialized object
    */
   public function setProgress($progress) {
      // TYPECHECK PARAMETERS
      if(!is_float($progress)) {
         throw new IllegalArgumentException(
               sprintf("Type of Field [progress], with current type [%s],".
                       " must be [float]",
               is_object($progress)?get_class($progress):gettype($progress)
         ));
      }
      //VALUECHECK PARAMETERS
      if($progress < 0.0 || $progress > 100.0) {
         throw new IllegalArgumentException(
                 sprintf("Value of Field [progress], with current value [%s],".
                         " must be between 0.0 and 100.0", $progress));
      }
      
      $this->progress = $progress;
      return $this;
   }
}

?>
