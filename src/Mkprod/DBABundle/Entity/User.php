<?php

namespace Mkprod\DBABundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Doctrine\ORM\Mapping as ORM;



/**
 * @ORM\Entity(repositoryClass="UserRepository")
 */
class User implements AdvancedUserInterface, EquatableInterface, \Serializable {
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer 
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=64, unique=true)
     * @var string
     */
    private $username;
    
   /**
    * @ORM\Column(type="string", length=64)
    * @var string
    */
    private $alias;
    
    /**
     * @ORM\Column(name="is_original", type="boolean")
     * @var boolean
     */
    private $isOriginal;
    
    /**
     * @ORM\Column(name="is_active", type="boolean")
     * @var boolean
     */
    private $isActive;
    
    /**
     * @ORM\Column(type="string", length=32)
     * @var string
     */
    private $salt;
    
    /**
     * @ORM\Column(type="string", length=128)
     * @var string 
     */
    private $password;
    
    /**
     * @ORM\Column(type="string", length=254, unique=true)
     * @var string
     */
    private $email;
    
    
    /**
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users")
     * @var type 
     */
    private $roles;
    
    
    
   /**
    * Constructor
    */
    public function __construct()
    {
        $this->roles = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    
    /**
     * @inheritDoc
     */
    public function eraseCredentials() {
    }

    /**
     * @inheritDoc
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * @inheritDoc
     */
    public function getRoles() {
        return $this->roles->toArray();
    }

    /**
     * @inheritDoc
     */
    public function getSalt() {
        return $this->salt;
    }

    /**
     * @inheritDoc
     */
    public function getUsername() {
        return $this->username;
    }
    
    /**
     * @inheritDoc
     */
    public function isAccountNonExpired() {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function isAccountNonLocked() {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function isCredentialsNonExpired() {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function isEnabled() {
        return $this->isActive;
    }    

    /**
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param integer $id
     * @return \Mkprod\DBABundle\Entity\User
     */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsOriginal() {
        return $this->isOriginal;
    }

    /**
     * @param boolean $isOriginal
     * @return \Mkprod\DBABundle\Entity\User
     */
    public function setIsOriginal($isOriginal) {
        $this->isOriginal = $isOriginal;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsActive() {
        return $this->isActive;
    }

    /**
     * @param boolean $isActive
     * @return \Mkprod\DBABundle\Entity\User
     */
    public function setIsActive($isActive) {
        $this->isActive = $isActive;
        return $this;
    }
    
    /**
     * Add groups
     *
     * @param \Mkprod\DBABundle\Entity\Role $groups
     * @return User
     */
    public function addGroup(\Mkprod\DBABundle\Entity\Role $groups)
    {
        $this->roles[] = $groups;
    
        return $this;
    }

    /**
     * Remove groups
     *
     * @param \Mkprod\DBABundle\Entity\Role $groups
     */
    public function removeGroup(\Mkprod\DBABundle\Entity\Role $groups)
    {
        $this->roles->removeElement($groups);
    }

    
    /**
     * Get groups
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGroups()
    {
        return $this->roles;
    }

    /**
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * @param string $email
     * @return \Mkprod\DBABundle\Entity\User
     */
    public function setEmail($email) {
        $this->email = $email;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getAlias() {
        return $this->alias;
    }

    /**
     * @param string $alias
     * @return \Mkprod\DBABundle\Entity\User
     */
    public function setAlias($alias) {
        $this->alias = $alias;
        return $this;
    }
    
    /**
     * Set username
     * @param string $username
     * @return User
     */
    public function setUsername($username) {
        $this->username = $username;
        return $this;
    }

    /**
     * Set salt
     * @param string $salt
     * @return User
     */
    public function setSalt($salt) {
        $this->salt = $salt;
        return $this;
    }

    /**
     * Set password
     * @param string $password
     * @return User
     */
    public function setPassword($password) {
        $this->password = $password;
        return $this;
    }

    /**
     * 
     * @param \Symfony\Component\Security\Core\User\UserInterface $user
     * @return boolean
     */
    public function isEqualTo(UserInterface $user) {
        // username is a unique property
        return $this->username === $user->getUsername();
    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize() {
        return serialize(array(
           $this->id 
        ));
    }
    
    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized) {
        list($this->id) = unserialize($serialized);
    }
}