<?php

namespace Mkprod\DBABundle\Entity;

use Symfony\Component\Security\Core\Role\RoleInterface;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 */
class Role implements RoleInterface {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=32, unique=true) 
     * @var string
     */
    private $role;
    
    /**
     * @ORM\Column(type="string", length=32)
     * @var string
     */
    private $name;
    
    
    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="roles")
     * @var type 
     */
    private $users;
    
    
    /**
     * @return string
     */
    public function getRole() {
        return $this->role;
    }    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set role
     *
     * @param string $role
     * @return Role
     */
    public function setRole($role)
    {
        $this->role = $role;
    
        return $this;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Role
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add users
     *
     * @param \Mkprod\DBABundle\Entity\User $users
     * @return Role
     */
    public function addUser(\Mkprod\DBABundle\Entity\User $users)
    {
        $this->users[] = $users;
    
        return $this;
    }

    /**
     * Remove users
     *
     * @param \Mkprod\DBABundle\Entity\User $users
     */
    public function removeUser(\Mkprod\DBABundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }
}