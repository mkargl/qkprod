<?php

namespace Blogger\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Blogger\BlogBundle\Entity\Comment;
use Blogger\BlogBundle\Form\CommentType;
use \Blogger\BlogBundle\Util\StringUtil;

/**
 * Comment controller.
 */
class CommentController extends Controller
{
    
    
    /**
     * Displays the comment form
     * @param integer $blog_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction($blog_id) {
        $blog = $this->getBlog($blog_id);

        $comment = new Comment();
        $comment->setBlog($blog);
        $form   = $this->createForm(new CommentType(), $comment);

        return $this->render('BloggerBlogBundle:Comment:form.html.twig', array(
            'comment' => $comment,
            'form'   => $form->createView()
        ));
    }

    
    
    /**
     * Submits the input comment data and persists them to database
     * @param type $blog_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction($blog_id) {
        $blog = $this->getBlog($blog_id);

        $comment  = new Comment();
        $comment->setBlog($blog);
        
        // bind commentform-reference to request
        $form    = $this->createForm(new CommentType(), $comment);
        $form->bind($this->getRequest());

        if ($form->isValid()) {
            $em = $this->getDoctrine()
                       ->getManager();
            $em->persist($comment);
            $em->flush();
               
            $slug = StringUtil::slugify($comment->getBlog()->getTitle());
            return $this->redirect($this->generateUrl('BloggerBlogBundle_blog_show', array(
                'id'    => $comment->getBlog()->getId(),
                'slug'  => $slug)) .
                '#comment-' . $comment->getId()
            );
        }

        return $this->render('BloggerBlogBundle:Comment:create.html.twig', array(
            'comment' => $comment,
            'form'    => $form->createView()
        ));
    }

    
    
    /**
     * 
     * @param type $blog_id
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     *         if blog entity could not be found
     */
    protected function getBlog($blog_id) {
        $em = $this->getDoctrine()
                    ->getManager();

        $blog = $em->getRepository('BloggerBlogBundle:Blog')->find($blog_id);

        if (!$blog) {
            throw $this->createNotFoundException('Unable to find Blog post.');
        }

        return $blog;
    }

}
