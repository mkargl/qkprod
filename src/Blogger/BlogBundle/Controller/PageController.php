<?php

namespace Blogger\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use Blogger\BlogBundle\Entity\Enquiry;
use Blogger\BlogBundle\Form\EnquiryType;

class PageController extends Controller {
    
    
    /**
     * Gets all the Blogs and renders them on screen
     * @return Symfony\Component\HttpFoundation\Response
     */
     public function indexAction() {
        $em = $this->getDoctrine()
                   ->getManager();
        
        $blogs = $em->getRepository("BloggerBlogBundle:Blog")
                    ->getLatestBlogs();
        
        return $this->render("BloggerBlogBundle:Page:index.html.twig", array(
            "blogs" => $blogs
        ));
    }
}
