<?php

namespace Blogger\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Blogger\BlogBundle\Entity\Blog;
use Blogger\BlogBundle\Form\BlogType;
use Blogger\BlogBundle\Util\StringUtil;


class BlogController extends Controller {
   
    
    
    /**
     * Renders the Blogform
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction() {
        $blog = new Blog();
        $blog->setBlog("")
             ->setImage("speechballoon.jpg")
             ->setTitle("")
             ->setTags("");

        $form = $this->createForm(new BlogType(), $blog);

        return $this->render("BloggerBlogBundle:Blog:form.html.twig", array(
            "form" => $form->createView()
        ));
    }
    
    
    /**
     * Persists the blogpost to database
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction() {
        $blog = new Blog();
        
        // bind userinput to object
        $form = $this->createForm(new BlogType(), $blog);
        $form->bind($this->getRequest());
        
        // get current username and set it as blogauthor
        $user= $this->get('security.context')
                    ->getToken()
                    ->getUser();        
        $blog->setAuthor($user->getUsername());
        
        // persist if input valid
        if($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($blog);
            $em->flush();
            
            return $this->redirect($this->generateUrl("BloggerBlogBundle_blog_show", array(
                "id"   => $blog->getId(),
                "slug" => StringUtil::slugify($blog->getTitle())
            )));
        }
        
        return $this->render("BloggerBlogBundle:Blog:create.html.twig", array(
           "form" => $form->createView()
        ));
    }
    
    
    
    /**
     * Shows one specific blog entry by its ID
     * @param integer $id   Primarykey of blogentry
     * @return \Symfony\Component\BrowserKit\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException 
     *         if no entries could have been found
     */
    public function showAction($id, $slug) {
        $em = $this->getDoctrine()->getManager();

        $blog = $em->getRepository('BloggerBlogBundle:Blog')->find($id);

        if (!$blog) {
            throw $this->createNotFoundException('Unable to find Blog post.');
        }

        $comments = $em->getRepository('BloggerBlogBundle:Comment')
                       ->getCommentsForBlog($blog->getId());

        return $this->render('BloggerBlogBundle:Blog:show.html.twig', array(
            'blog'      => $blog,
            'comments'  => $comments
        ));
    }
   
}
