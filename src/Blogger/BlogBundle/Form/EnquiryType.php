<?php
namespace Blogger\BlogBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\FieldType;
use Symfony\Component\Form\FormBuilderInterface;
    
    
/**
 * Description of EnquiryType
 *
 * @author michi
 */
class EnquiryType extends FieldType {
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add("name", "text")
                ->add("email", "email")
                ->add("subject", "text")
                ->add("body", "textarea");
    }
    
    public function getName() {
        return "contact";
    }
    
}

?>
