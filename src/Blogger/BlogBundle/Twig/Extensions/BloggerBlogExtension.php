<?php

namespace Blogger\BlogBundle\Twig\Extensions;

/**
 * Description of DateExtention
 *
 * @author michi
 */
class BloggerBlogExtension extends \Twig_Extension {

    public function getFilters() {
        return array(
            'ago'     => new \Twig_Filter_Method($this, 'ago'),
            'slugify' => new \Twig_Filter_Method($this, 'slugify') 
        );
    }

    
    /**
     * Turnes a date into a relative string
     * Example:  Today:    01.04.2013
     *           dateTime: 03.04.2013
     *           returns:  "2 days ago"
     * @param \DateTime $dateTime
     * @return string   string relative to todays date
     * @throws \InvalidArgumentException
     *         if the dateTime is not smaller than todays date
     */
    public function ago(\DateTime $dateTime) {
        return \Blogger\BlogBundle\Util\StringUtil::ago($dateTime);
    }
    
    
    
    /**
     * Slugifies text for SEO
     * @param text to change $text
     * @return string slugified string
     */
    public function slugify($text) {
        return \Blogger\BlogBundle\Util\StringUtil::slugify($text);
    }
    
    

    public function getName() {
        return 'blogger_blog_extension';
    }

}

?>
