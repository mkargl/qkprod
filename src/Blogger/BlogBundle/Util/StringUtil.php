<?php
namespace Blogger\BlogBundle\Util;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StringUtil
 *
 * @author michi
 */
class StringUtil {
    
    
    /**
     * Slugifies text for SEO
     * @param text to change $text
     * @return string slugified string
     */
    public static function slugify($text) {
        
        // replace non letter or digits by -
        $text = preg_replace('#[^\\pL\d]+#u', '-', $text);

        // trim
        $text = trim($text, '-');

        // transliterate
        if (function_exists('iconv')) {
            $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        }

        // lowercase
        $text = strtolower($text);

        // remove unwanted characters
        $text = preg_replace('#[^-\w]+#', '', $text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
    
    
    
    
    
        /**
     * Turnes a date into a relative string
     * Example:  Today:    01.04.2013
     *           dateTime: 03.04.2013
     *           returns:  "2 days ago"
     * @param \DateTime $dateTime
     * @return string   string relative to todays date
     * @throws \InvalidArgumentException
     *         if the dateTime is not smaller than todays date
     */
    public static function ago(\DateTime $dateTime) {
        $now = new \DateTime();
        $delta = $now->getTimestamp() - $dateTime->getTimestamp();
        if ($delta < 0)
            throw new \InvalidArgumentException(
                    "createdAgo is unable to handle dates in the future:
                        now[".$now->getTimestamp()."]- postdate[".$dateTime->getTimestamp()."]");

        $duration = "";
        if ($delta < 60) {
            // Seconds
            $time = $delta;
            $duration = $time . " second" . (($time > 1) ? "s" : "") . " ago";
        } else if ($delta <= 3600) {
            // Mins
            $time = floor($delta / 60);
            $duration = $time . " minute" . (($time > 1) ? "s" : "") . " ago";
        } else if ($delta <= 86400) {
            // Hours
            $time = floor($delta / 3600);
            $duration = $time . " hour" . (($time > 1) ? "s" : "") . " ago";
        } else {
            // Days
            $time = floor($delta / 86400);
            
            // calculate years
            if(($years = floor($time/365)) > 0) {
                $duration = "$years year". (($years>1)?"s ":" ");
                $time-=($years*356);
            }
            $duration .= $time . " day" . (($time > 1) ? "s" : "") . " ago";
        } 

        return $duration;
    }
}

?>
